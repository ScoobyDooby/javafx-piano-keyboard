/*
 *  Copyright � 2023 <John Marshall jm.dev@jmsoft.co.uk>
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of
 *  this software and associated documentation files (the �Software�), to deal in 
 *  the Software without restriction, including without limitation the rights to 
 *  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 *  the Software, and to permit persons to whom the Software is furnished to do so, 
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all 
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED �AS IS�, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 *  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
 *  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
 *  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
 *  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package uk.co.jmsoft.javafx.piano.keyboard;

import java.util.Optional;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.layout.*;
import uk.co.jmsoft.java.sound.utils.KeyColour;
import uk.co.jmsoft.java.sound.utils.Note;
import uk.co.jmsoft.java.sound.utils.NoteName;

public class PianoKeyboard extends StackPane
{

    private double whiteBlackWidthRatio = 15.0 / 23.0;
    private double whiteBlackHeightRatio = 88.0 / 136.0;
    private double blackKeyLeftRightAlignmentRatio = 2.5 / 4.0;
    private final int keyCount;
    private final Note bottomNote;
    private Pane whiteKeyPane;
    private Pane blackKeyPane;
    private NotePlayedHandler notePlayedHandler;
    private NoteLabelStyle noteLabelStyle = NoteLabelStyle.NONE;

    /**
     * Creates a standard 88 key piano keyboard.
     *
     * @param notePlayedHandler
     */
    public PianoKeyboard(NotePlayedHandler notePlayedHandler)
    {
        this(Note.A0, 88, notePlayedHandler);
    }

    /**
     * Create a piano keyboard with the given low note and number of keys. The
     * highest key on the piano must not be higher than G9. The lowest note on
     * the piano must not be less than C-1.
     *
     * @param bottomNote The lowest note on the keyboard. Must be between C-1
     * and G9.
     * @param keyCount The number of keys on the keyboard. Must be chosen to
     * ensure the top note is not higher than G9.
     * @param notePlayedHandler
     */
    public PianoKeyboard(Note bottomNote, int keyCount, NotePlayedHandler notePlayedHandler)
    {
        this.bottomNote = bottomNote;
        this.keyCount = keyCount;
        this.notePlayedHandler = notePlayedHandler;
        // White keys pane
        whiteKeyPane = new Pane();
        whiteKeyPane.setMinHeight(0);
        this.getChildren().add(whiteKeyPane);
        // Black key pane
        VBox blackKeyVBox = new VBox();
        blackKeyVBox.setFillWidth(true);
        this.getChildren().add(blackKeyVBox);
        blackKeyPane = new Pane();
        blackKeyPane.setMinHeight(0);
        blackKeyPane.setPickOnBounds(false);
        blackKeyVBox.getChildren().add(blackKeyPane);
        blackKeyVBox.setPickOnBounds(false);

        // Create buttons
        Note currentNote = bottomNote;

        for (int i = 0; i < keyCount; i++)
        {
            Button newButton = new Button("");
            newButton.setUserData(currentNote);
            newButton.onActionProperty().setValue(new KeyEventHandler());
            newButton.setPickOnBounds(false);
            newButton.setMinWidth(0);
            newButton.setMinHeight(0);
            newButton.setMaxWidth(Double.MAX_VALUE);
            newButton.setMaxHeight(Double.MAX_VALUE);
            newButton.setFocusTraversable(false);

            if (currentNote.getNoteName().getKeyColour().equals(KeyColour.WHITE))
            {
                newButton.getStyleClass().add("white-key");
                whiteKeyPane.getChildren().add(newButton);
            }
            else
            {
                newButton.getStyleClass().add("black-key");
                blackKeyPane.getChildren().add(newButton);
            }

            Optional<Note> nextNote = currentNote.getNoteAbove();
            if (nextNote.isEmpty())
            {
                System.out.println("No note above");
                break;
            }
            currentNote = currentNote.getNoteAbove().get();
        }
    }

    public double getWhiteBlackWidthRatio()
    {
        return whiteBlackWidthRatio;
    }

    public void setWhiteBlackWidthRatio(double whiteBlackWidthRatio)
    {
        this.whiteBlackWidthRatio = whiteBlackWidthRatio;
    }

    public double getWhiteBlackHeightRatio()
    {
        return whiteBlackHeightRatio;
    }

    public void setWhiteBlackHeightRatio(double whiteBlackHeightRatio)
    {
        this.whiteBlackHeightRatio = whiteBlackHeightRatio;
    }

    /**
     * Get the fraction of the black key that overlaps the neighbouring white
     * key.
     *
     * @return The fraction.
     */
    public double getBlackKeyLeftRightAlignmentRatio()
    {
        return blackKeyLeftRightAlignmentRatio;
    }

    /**
     * Set the fraction of the black key that overlaps the neighbouring white
     * key.
     *
     * @param blackKeyLeftRightAlignmentRatio The ratio.
     */
    public void setBlackKeyLeftRightAlignmentRatio(double blackKeyLeftRightAlignmentRatio)
    {
        this.blackKeyLeftRightAlignmentRatio = blackKeyLeftRightAlignmentRatio;
    }

    @Override
    protected void layoutChildren()
    {
        int whiteKeyCount = whiteKeyPane.getChildren().size();
        double whiteKeyWidth = this.getWidth() / whiteKeyCount;
        double blackKeyWidth = whiteKeyWidth * whiteBlackWidthRatio;
        double halfBlackKeyWidth = blackKeyWidth / 2;
        double blackKeyHeight = this.getHeight() * whiteBlackHeightRatio;

        whiteKeyPane.setPrefWidth(this.getWidth());
        whiteKeyPane.setPrefHeight(this.getHeight());
        blackKeyPane.setPrefWidth(this.getWidth());
        blackKeyPane.setPrefHeight(this.getHeight());

        // Layout black keys
        Optional<Note> currentNote = Optional.ofNullable(bottomNote);
        int currentWhiteKey = -1;
        int currentBlackKey = -1;

        for (int i = 0; i < keyCount && currentNote.get().getNoteAbove().isPresent(); i++, currentNote = currentNote.get().getNoteAbove())
        {
            if (currentNote.get().getNoteName().getKeyColour().equals(KeyColour.WHITE))
            {
                currentWhiteKey++;
                long xPos = Math.round(currentWhiteKey * whiteKeyWidth);
                long xPosRight = Math.round((currentWhiteKey + 1) * whiteKeyWidth);
                long pixelWidth = xPosRight - xPos;

                Button button = (Button) whiteKeyPane.getChildren().get(currentWhiteKey);
                button.relocate(xPos, 0);
                button.setPrefWidth(pixelWidth);
                button.setPrefHeight(whiteKeyPane.getHeight());
                if (noteLabelStyle == NoteLabelStyle.LETTER)
                {
                    button.setText(currentNote.get().getNoteName().getReadableName());
                }
                else if (noteLabelStyle == NoteLabelStyle.SCIENTIFIC)
                {
                    button.setText(currentNote.get().getReadableName());
                }
            }
            else
            {
                // black key
                // A black keys xpos is relative to the white key above it.
                // white keys are zero indexed.
                currentBlackKey++;
                double alignmentOffset;
                if (currentNote.get().getNoteName().getKeyAlignment().equals(NoteName.KeyAlignment.LEFT))
                {
                    alignmentOffset = -blackKeyWidth * blackKeyLeftRightAlignmentRatio;
                }
                else if (currentNote.get().getNoteName().getKeyAlignment().equals(NoteName.KeyAlignment.RIGHT))
                {
                    alignmentOffset = -blackKeyWidth * (1 - blackKeyLeftRightAlignmentRatio);
                }
                else // Center
                {
                    alignmentOffset = -halfBlackKeyWidth;
                }
                long xPos = Math.round((currentWhiteKey + 1) * whiteKeyWidth + alignmentOffset);
                long xPosRight = Math.round(blackKeyWidth + xPos);
                long pixelWidth = xPosRight - xPos;
                Button button = (Button) blackKeyPane.getChildren().get(currentBlackKey);
                button.relocate(xPos, 0);
                button.setPrefWidth(pixelWidth);
                button.setPrefHeight(blackKeyHeight);
                if (noteLabelStyle == NoteLabelStyle.LETTER)
                {
                    button.setText(currentNote.get().getNoteName().getReadableName());
                }
                else if (noteLabelStyle == NoteLabelStyle.SCIENTIFIC)
                {
                    button.setText(currentNote.get().getReadableName());
                }
            }

        }
        super.layoutChildren();
    }

    public int getKeyCount()
    {
        return keyCount;
    }

    public Note getBottomNote()
    {
        return bottomNote;
    }

    private class KeyEventHandler implements EventHandler<ActionEvent>
    {

        @Override
        public void handle(ActionEvent event)
        {
            Button button = (Button) event.getSource();
            Note note = (Note) button.getUserData();
            notePlayedHandler.onNotePlayed(button, note);
        }
    }

    public enum NoteLabelStyle
    {
        NONE,
        SCIENTIFIC,
        LETTER
    }

    public void setNoteLabelStyle(NoteLabelStyle noteLabelStyle)
    {
        this.noteLabelStyle = noteLabelStyle;
    }
}
