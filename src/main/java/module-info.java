module uk.co.jmsoft.javafx.piano.keyboard
{
    exports uk.co.jmsoft.javafx.piano.keyboard;
    exports uk.co.jmsoft.javafx.piano.keyboard.example;

    requires javafx.controls;
    requires uk.co.jmsoft.java.sound.utils;
}