package uk.co.jmsoft.javafx.piano.keyboard.example;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import uk.co.jmsoft.javafx.piano.keyboard.NotePlayedHandler;
import uk.co.jmsoft.javafx.piano.keyboard.PianoKeyboard;

import uk.co.jmsoft.java.sound.utils.Note;
public class FXMain extends Application {


    @Override
    public void start(Stage stage){

        NotePlayedHandler notePlayedHandler = new NotePlayedHandler()
        {
            @Override
            public void onNotePlayed(Button button, Note note)
            {
                System.out.println("Played " + note.name());
            }
        };
        PianoKeyboard root = new PianoKeyboard(notePlayedHandler);
        root.setNoteLabelStyle(PianoKeyboard.NoteLabelStyle.LETTER);
        //Parent root = new PianoKeyboard(new Note(NoteName.A_SHARP_B_FLAT, 2),12, notePlayedHandler);
        Scene scene = new Scene(root, 800,100);
        scene.getStylesheets().add("pianoKeyboardStyle.css");
        stage.setTitle("JavaFX piano keyboard example");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }


}